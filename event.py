from app import app, db
from app.models import User, EventPost, PostLike
@app.shell_context_processor
def make_shell_context():
  return {'db': db, 'User': User, 'EventPost' : EventPost, 'PostLike' : PostLike}
