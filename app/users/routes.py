from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from app import db, app
from werkzeug.security import generate_password_hash,check_password_hash
from app.models import User, EventPost
from app.users.forms import RegistrationForm, LoginForm, UpdateUserForm
from app.users.picture_handler import add_profile_pic
from werkzeug.utils import secure_filename
import os
from config import Config

UPLOAD_FOLDER = './app/static/images'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
print(UPLOAD_FOLDER)



users = Blueprint('users', __name__)

@users.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()

    if form.validate_on_submit():
      file = request.files['file']
      if file.filename == '':
        flash('No image selected')
        return redirect(request.url)
      if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        user = User(email=form.email.data,
                    username=form.username.data,
                    password=form.password.data,
                    profile_image=os.path.join('../static/images', filename))

        db.session.add(user)
        db.session.commit()
        flash('Thanks for registering! Now you can login!')
        return redirect(url_for('users.login'))
    return render_template('register.html', form=form)

@users.route('/login', methods=['GET', 'POST'])
def login():

    form = LoginForm()
    if form.validate_on_submit():

        user = User.query.filter_by(email=form.email.data).first()

        if user is not None and user.check_password(form.password.data):

            login_user(user)
            flash('Logged in successfully.')

            next = request.args.get('next')

            if next == None or not next[0]=='/':
                next = url_for('core.index')

            return redirect(next)
    return render_template('login.html', form=form)




@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('core.index'))


@users.route("/account", methods=['GET', 'POST'])
@login_required
def account():

    form = UpdateUserForm()

    if form.validate_on_submit():
        print(form)
        if form.picture.data:
            username = current_user.username
            pic = add_profile_pic(form.picture.data,username)
            current_user.profile_image = pic

        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('User Account Updated')
        return redirect(url_for('users.account'))

    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email

    profile_image = url_for('static', filename='profile_pics/' + current_user.profile_image)
    return render_template('account.html', profile_image=profile_image, form=form)


@users.route("/<username>")
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    event_posts = EventPost.query.filter_by(author=user).order_by(EventPost.date.desc()).paginate(page=page, per_page=5)
    return render_template('user_event_posts.html', event_posts=event_posts, user=user)





def allowed_file(filename):
   return '.' in filename and \
           filename.split('.')[-1].lower() in ALLOWED_EXTENSIONS

