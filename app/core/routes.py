from flask import render_template, request, Blueprint
from flask import url_for, redirect, send_file, jsonify
from app.models import EventPost
from flask_login import current_user, login_required
from datetime import datetime
from app import app, db
from flask import current_app
from app.task import create_file_in_background
from flask import g
from app.core.forms import SearchForm
import psycopg2


core = Blueprint('core', __name__)


@core.route('/')
def index():
    page = request.args.get('page', 1, type=int)
    event_posts = EventPost.query.order_by(EventPost.date.desc()).paginate(page=page, per_page=10)
    return render_template('index.html',event_posts=event_posts)


@core.route('/info')
def info():
    return render_template('info.html')


@core.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@core.before_request
def before_request_s():
    g.search_form = SearchForm()


@core.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('core.index'))
    page = request.args.get('page', 1, type=int)
    posts, total = EventPost.search(g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('core.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('core.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title='Search', posts=posts,
                           next_url=next_url, prev_url=prev_url)


@core.route('/download_content', methods=['GET', 'POST'])
def download_content():
    task = create_file_in_background.apply_async()
    return jsonify({}), {'location': url_for('core.check_status', task_id=task.id)}


@core.route('/status/<task_id>', methods=['GET', 'POST'])
def check_status(task_id):
    task = create_file_in_background.AsyncResult(task_id)
    response = {
    'state': task.state
    }
    return jsonify(response)


@core.route('/download_start', methods=['GET'])
def download_start():
    return send_file('../a.json', as_attachment=True, cache_timeout=0)
