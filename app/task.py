from celery import Celery
from app import celery
from app.models import EventPost
from flask import json
import time
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
# from app import app, db

@celery.task(bind=True)
def create_file_in_background(self):
  app = Flask(__name__)
  db = SQLAlchemy(app)
  app.config.from_object(Config)

  print('3')
  with app.app_context():
    print('4')
    dic ={}
    event_post_list = EventPost.query.all()
    print(event_post_list)
    for event in event_post_list:
      dic[event.id]={}
      dic[event.id]['event-content']= event.title

    print('4')

    print('5')
    file = open('a.json','w')
    file.write(json.dumps(dic))
    file.close()
    print('6')
  return 'success'
