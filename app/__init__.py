import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from config import Config
from elasticsearch import Elasticsearch
from celery import Celery


db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
login_manager.login_view = "users.login"


app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
migrate.init_app(app, db)
login_manager.init_app(app)

app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
      if app.config['ELASTICSEARCH_URL'] else None



app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)










@login_manager.user_loader
def load_user(user_id):
    return None




from app.users.routes import users
app.register_blueprint(users)

from app.event_posts.routes import event_posts
app.register_blueprint(event_posts)

from app.error_pages.handlers import error_pages
app.register_blueprint(error_pages)



from app.core.routes import core
app.register_blueprint(core)


