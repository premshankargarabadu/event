from flask import Flask,render_template,url_for,flash, redirect,request,Blueprint
from flask_login import current_user,login_required
from app import db, app
from app.models import EventPost,Comment
from app.event_posts.forms import EventPostForm , AddCommentForm
from werkzeug.utils import secure_filename
import os

UPLOAD_FOLDER = './app/static/images'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])



event_posts = Blueprint('event_posts',__name__)

@event_posts.route('/create',methods=['GET','POST'])
@login_required
def create_post():
    form = EventPostForm()

    if form.validate_on_submit():
      file = request.files['image']
      if file.filename == '':
        flash('No image selected')
        return redirect(request.url)
      if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        event_post = EventPost(title=form.title.data, text=form.text.data, event_image=os.path.join('../static/images', filename), user_id=current_user.id)
        db.session.add(event_post)
        db.session.commit()
        flash("Event Post Created")
        return redirect(url_for('core.index'))

    return render_template('create_post.html',form=form)


@event_posts.route('/<int:event_post_id>')
def event_post(event_post_id):
    event_post = EventPost.query.get_or_404(event_post_id)
    event_comment = Comment.query.filter_by(event_id=event_post_id)
    print(event_comment)
    return render_template('event_post.html',title=event_post.title,
                            date=event_post.date,post=event_post,event_comment=event_comment
    )

@event_posts.route("/<int:event_post_id>/update", methods=['GET', 'POST'])
@login_required
def update(event_post_id):
    event_post = EventPost.query.get_or_404(event_post_id)

    event_comment = Comment.query.filter_by(event_id=event_post_id).all()

    if event_post.author != current_user:
        abort(403)

    form = EventPostForm()
    if form.validate_on_submit():
        event_post.title = form.title.data
        event_post.text = form.text.data
        # event_post.event_image = form.image.data
        db.session.commit()
        flash('Post Updated')
        return redirect(url_for('event_posts.event_post', event_post_id=event_post.id))
    elif request.method == 'GET':
        form.title.data = event_post.title
        form.text.data = event_post.text
    return render_template('create_post.html', title='Update',
                           form=form)


@event_posts.route("/<int:event_post_id>/delete", methods=['POST'])
@login_required
def delete_post(event_post_id):
    event_post = EventPost.query.get_or_404(event_post_id)
    comment_post = event_post.comments.all()
    if event_post.author != current_user:
        abort(403)
    db.session.delete(event_post)
    for comment in comment_post:
        db.session.delete(comment)
    db.session.commit()
    flash('Post has been deleted')
    return redirect(url_for('core.index'))


@event_posts.route('/like/<int:post_id>/<action>')
@login_required
def like_action(post_id, action):
    post = EventPost.query.filter_by(id=post_id).first_or_404()
    if action == 'like':
        current_user.like_post(post)
        db.session.commit()
    if action == 'unlike':
        current_user.unlike_post(post)
        db.session.commit()
    return redirect(request.referrer)




def allowed_file(filename):
   return '.' in filename and \
           filename.split('.')[-1].lower() in ALLOWED_EXTENSIONS






from flask import request

@event_posts.route("/post/<int:post_id>/comment", methods=["GET", "POST"])
@login_required
def comment_post(post_id):
    post = EventPost.query.get_or_404(post_id)
    form = AddCommentForm()
    if request.method == 'POST': # this only gets executed when the form is submitted and not when the page loads
        if form.validate_on_submit():
            comment = Comment(body=form.body.data, article=post, comment_author=current_user)
            db.session.add(comment)
            db.session.commit()
            flash("Your comment has been added to the post", "success")
            return redirect(url_for("event_posts.event_post", event_post_id=post.id))
    return render_template("comment_post.html", title="Comment Post",
form=form, post_id=post_id)
